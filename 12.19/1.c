#include<stdio.h>
#include<string.h>
struct student
{
   long   num;             /*  学号  */
   char   *name={'\0'};    /*  姓名  */
   char   *gender={'\0'};  /*  性别  */
   int    age;             /*  年龄  */
   float  score;           /*  成绩  */
   char   *addr={'\0'};    /*  地址  */
};

main()
{
   char   *char_temp[2];
   long   long_temp=0;
   int    int_temp=0;
   float  float_temp=0;
   /*   temp变量用于在赋值前检查合法性   */
   int    i,l,s;

   struct student n1;

   clrscr();

   /*  学号  */
   while(1)
   {
	printf("\nPlease input the number of the student(less than 9bit): ");
	scanf("%ld",&long_temp);
	if(long_temp<10000000000&&long_temp>0)
       {
		 n1.num=long_temp;
		 long_temp=0;
		 break;
	   }
    else
	printf("\nInput Error!\nPlease input again!");
	clrscr();
   }

    /*  姓名  */
   while(1)
   {
    printf("\nPlease input the number of the student(less than 20bit): ");
    fflush(stdin);
    gets(char_temp[0]);
	l=strlen(char_temp[0]);
    if(l<21)
	   {
         for(i=0,s=0;i<l;i++)
           {
		      if(*(char_temp[0]+i)<'A'||*(char_temp[0]+i)>'Z'&&*(char_temp[0]+i)<'a'||*(char_temp[0]+i)>'z')
	              if(*(char_temp[0]+i)!=' ')
	                 s=1;
		   }
       }
    if(s!=1)
	{
	 n1.name=char_temp[0];
	 break;
	}
	else
	clrscr();
    printf("\nInput Error!\nPlease input again!");
   }

    /*  性别  */
   while(1)
   {
    printf("\nPlease input the number of the student(use F and M): ");
    fflush(stdin);
	gets(char_temp[1]);
	if(*char_temp[1]=='F'||*char_temp[1]=='M')
	{
	 n1.gender=char_temp[1];
	 break;
	}
	else
	clrscr();
    printf("\nInput Error!\nPlease input again!");
   }

    /*  分数  */
   while(1)
   {
    printf("\nPlease input the number of the student(less than 150 and bigger than 0): ");
    scanf("%d",&float_temp);
    if(float_temp<150&&float_temp>0)
       {
         n1.score=float_temp;
		 float_temp=0;
		 break;
	   }
    else
	printf("\nInput Error!\nPlease input again!");
	clrscr();
   }

    /*  地址  */
   while(1)
   {
    printf("\nPlease input the number of the student(less than 50bit): ");
    fflush(stdin);
    gets(char_temp[2]);
	l=strlen(char_temp[2]);
    if(l<50)
	   {
         for(i=0,s=0;i<l;i++)
           {
		      if(*(char_temp[2]+i)==' '||*(char_temp[2]+i)>='1'&&*(char_temp[2]+i)<='9'||*(char_temp[2]+i)=='#')
			    continue;
				 else
		            if(*(char_temp[2]+i)<'A'||*(char_temp[2]+i)>'Z'&&*(char_temp[2]+i)<'a'||*(char_temp[2]+i)>'z')
	                   s=1;
		   }
       }
    if(s!=1)
	{
	 n1.addr=char_temp[2];
	 break;
	}
	else
	clrscr();
    printf("\nInput Error!\nPlease input again!");
   }

    /*  输出  */
	printf("%ld",n1.num);
	printf("%s",n1.name);
	printf("%s",n1.gender);
	printf("%f",n1.score);
	printf("%s",n1.addr);
}