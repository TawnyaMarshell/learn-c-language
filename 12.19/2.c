#include<stdio.h>
#include<string.h>
struct student
{
   long   num;           /*  学号  */
   char   name[20];      /*  姓名  */
   char   gender;        /*  性别  */
   int    age;           /*  年龄  */
   float  score;         /*  成绩  */
   char   addr[50];      /*  地址  */
};

main()
{
   struct student n1;

   clrscr();

    /*  学号  */
	printf("\nPlease input the ID of the student(less than 9bit): ");
	scanf("%ld",&n1.num);

    /*  姓名  */
	printf("\nPlease input the NAME of the student(less than 20bit): ");
    fflush(stdin);
    gets(n1.name);

    /*  性别  */
	printf("\nPlease input the GENDER of the student(use F and M): ");
    fflush(stdin);
	gets(n1.gender);

    /*  分数  */
	printf("\nPlease input the DCORE of the student(less than 150 and bigger than 0): ");
    scanf("%f",&n1.score);

    /*  地址  */
	printf("\nPlease input the ADDRESS of the student(less than 50bit): ");
    fflush(stdin);
    gets(n1.addr);

    /*  输出  */
	printf("THE ID OF THE STUDENT IS %ld\n",n1.num);
	printf("THE NAME OF THE STUENT IS %s\n",n1.name);
	printf("THE GENDER OF THE STUDENT IS %s\n",n1.gender);
	printf("THE SCORE OF THE STUDENT IS %f\n",n1.score);
	printf("THE ARDDRESS OF THE STUDENT IS %s\n",n1.addr);
}