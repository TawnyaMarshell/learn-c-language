#include<stdio.h>
typedef struct student
{
   char num[20];
   char name[20];
   int age;
   float score;
}st;

main()
{
   st n1={"12160600209","Liuminakai",21,99.9};
   st *p;
   clrscr();
   p=&n1;   /* &用于取n1的地址，p指向n1的地址 */
   printf("     ID          Name       Age     Score  \n");
   printf(" %s  %s     %d     %.2f",p->num,p->name,p->age,p->score);
}