#include <stdio.h>
main(){
	int iOperand1 = 0;
	int iOperand2 = 0;
	int iResult = 0;

	clrscr();
	printf("\n\tAdder Program, by Michael Vine\n");
	printf("\nEnter first operand: ");
	scanf("%d",&iOperand1);
	printf("\nEnter second operand: ");
	scanf("%d",&iOperand2);
	iResult = iOperand1 + iOperand2;

	printf("\nThe result is %d\n", iResult);
}